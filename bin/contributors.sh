#!/bin/sh

git log --format=%aN | sort | uniq | while read X
do
	printf -- "- %s\n" "${X}"
done > _data/contributors.yml
