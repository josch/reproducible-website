---
layout: default
title: Who is involved?
permalink: /who/
order: 40
---

# Individuals involved in Reproducible Builds

← *Back to [who is involved]({{ "/who/" | relative_url }})*

<br>

Various individuals are working on providing reproducible builds to their users and developers via the not-for-profit Reproducible Builds project.
{: .lead}

<br>

<div class="row">
<div class="col-md-6 text-center" markdown="1">

<a href="{{ "/who/projects/" | relative_url }}"><img src="{{ "/images/who/projects.svg" | relative_url }}" height="200"></a>

[Projects]({{ "/who/projects/" | relative_url }})
{: .lead}

</div>
<div class="col-md-6 text-center" markdown="1">

<a href="{{ "/who/people/" | relative_url }}"><img src="{{ "/images/who/people.svg" | relative_url }}" height="200"></a>

[People]({{ "/who/people/" | relative_url }})
{: .lead}

</div>
</div>

<br>

## Sponsors

You can also see [who is currently supporting]({{ "/sponsors" | relative_url }}) the Reproducible Builds project. If you are interested in our work, please consider [becoming a sponsor]({{ "/donate/" | relative_url }}).
