---
layout: default
title: Who is involved?
permalink: /who/people/
---

# Who is involved?

← Back to [who is involved]({{ "/who/" | relative_url }})

Various free software projects are working on providing reproducible builds to their users and developers via the not-for-profit Reproducible Builds project.
{: .lead}

## Core team

* [Chris Lamb](https://chris-lamb.co.uk) (`lamby`)
* [Holger Levsen](http://layer-acht.org/thinking/) (`h01ger`)
* [Mattia Rizzolo](https://mapreri.org/) (`mapreri`)
* [Vagrant Cascadian](https://www.aikidev.net/about/story/) (`vagrantc`)

The 'core team' consists of those who have been working consistently towards
reproducible builds for many years on both the technical aspects of the project
as well as participating in significant networking around the community itself.
Their work is funded by the Reproducible Builds project via the
[Software Freedom Conservancy](https://sfconservancy.org/).)

The preferred way to contact the team is to post to [our public mailing list, `rb-general`](https://lists.reproducible-builds.org/listinfo/rb-general). However, if you wish to contact the core team directly, please email [`contact@reproducible-builds.org`](mailto:contact@reproducible-builds.org).

## Steering Committee

* [Allen Gunn](https://aspirationtech.org)
* [Bdale Garbee](http://gag.com/bdale/)
* [Holger Levsen](http://layer-acht.org/thinking/)
* [Keith Packard](https://keithp.com)
* [Mattia Rizzolo](https://mapreri.org/)
* [Stefano Zacchiroli](https://upsilon.cc/)

The Steering Committee acts as the board of the Reproducible Builds project within the [Software Freedom Conservancy](https://sfconservancy.org/). Its main purpose is deciding how to spend project money.

{% if site.data.contributors %}
## Contributors

{% for x in site.data.contributors %}{{ x }}{% unless forloop.last %}, {% endunless %}{% endfor %}.
{% endif %}

## Sponsors

You can see [who is currently supporting]({{ "/sponsors" | relative_url }}) the Reproducible Builds project, and if you are interested in our work please consider [becoming a sponsor]({{ "/donate/" | relative_url }}).
