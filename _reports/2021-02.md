---
layout: report
year: "2021"
month: "02"
title: "Reproducible Builds in February 2021"
draft: false
date: 2021-03-05 14:57:42
---

[![]({{ "/images/reports/2021-02/reproducible-builds.png#right" | relative_url }})](https://reproducible-builds.org/)

**Welcome to the report from the [Reproducible Builds](https://reproducible-builds.org) project for February 2021.** In our monthly reports, we try to outline the most important things that have happened in the world of reproducible builds. If you are interested in contributing to the project, though, please visit our [*Contribute*]({{ "/contribute/" | relative_url }}) page on [our website]({{ "/" | relative_url }}).

## Community news

[![]({{ "/images/reports/2021-02/gnu-mes-talk.png#right" | relative_url }})](https://fosdem.org/2021/schedule/event/gnumes/)

On Sunday 7th February, Jan '*janneke*' Nieuwenhuizen gave a talk at [FOSDEM '21](https://fosdem.org/2021/) on [GNU Mes](https://www.gnu.org/software/mes/): [*Reproducibility is not enough: The missing link between stage0/M2-Planet and Mes*](https://fosdem.org/2021/schedule/event/gnumes/). Taking place in the [Declarative and Minimalistic Computing devroom](https://fosdem.org/2021/schedule/track/declarative_and_minimalistic_computing/), Jan's talk touched on reproducible builds and how a minimal binary seed further reduces the security attack surface when creating (or "bootstrapping") a system from scratch.

<br>

A few days earlier, Eric Brewer, Rob Pike, Abhishek Arya, Anne Bertucio and Kim Lewandowski wrote a post on the [Google Security Blog](https://security.googleblog.com/) proposing an industry-wide framework they call "[*Know, Prevent, Fix*](https://security.googleblog.com/2021/02/know-prevent-fix-framework-for-shifting.html)" which aims to improve how the industry might think about vulnerabilities in open source software, including "Consensus on metadata and identity standards" and — more relevant to the Reproducible Builds project — "Increased transparency and review for critical software":

> Ken Thompson's Turing Award lecture famously demonstrated in 1984 that authentic source code alone is not enough, and recent events have shown this attack is a real threat. How do you trust your build system? All the components of it must be trusted and verified through a continuous process of building trust. Reproducible builds help—there is a deterministic outcome for the build and we can thus verify that we got it right—but are harder to achieve due to ephemeral data (such as timestamps) ending up in the release artifact. And safe reproducible builds require verification tools, which in turn must be built verifiably and reproducibly, and so on. We must construct a network of trusted tools and build products.&nbsp;[[...](https://security.googleblog.com/2021/02/know-prevent-fix-framework-for-shifting.html)]

<br>

After that, [Drew DeVault](https://drewdevault.com/) wrote an interesting blog post titled [*How to make your downstream users happy*](https://drewdevault.com/2021/02/09/How-to-make-your-downstreams-happy.html), pointing out that "There are a number of things that your FOSS project can be doing which will make the lives of your downstream users easier, particularly if you’re writing a library or programmer-facing tooling". We concur, especially with Drew's recommendations to use the Reproducible Builds' [`SOURCE_DATE_EPOCH`]({{ "/docs/source-date-epoch/" | relative_url }}) environment variable.

<br>

[![]({{ "/images/reports/2021-02/birsan.png#right" | relative_url }})](https://medium.com/@alex.birsan/dependency-confusion-4a5d60fec610)

Another blog post this month was written by [Alex Birsan](https://twitter.com/alxbrsn) where he [details a novel supply-chain attack](https://medium.com/@alex.birsan/dependency-confusion-4a5d60fec610), similar to (but also distinct from) the various [typo-squatting attacks](https://en.wikipedia.org/wiki/Typosquatting) that have been increasingly popular in the past year or so. Alex's post begins with the ominous phrase: "Ever since I started learning how to code, I have been fascinated by the level of trust we put [in] `pip install package_name`".

<br>

[![]({{ "/images/reports/2021-02/intoto.png#right" | relative_url }})](https://in-toto.io/)

Closer to home, Justin Cappos replied to an email on our mailing list answering the question [*How we could accelerate deployment of verified reproducible builds?*](https://lists.reproducible-builds.org/pipermail/rb-general/2021-February/002183.html), describing some of the workings of [`in-toto`](https://in-toto.io/) with regards to the potentially distributed validation of binary signatures.&nbsp;[[...](https://lists.reproducible-builds.org/pipermail/rb-general/2021-February/002183.html)]

<br>

## Software development

### [*diffoscope*](https://diffoscope.org)

[![]({{ "/images/reports/2021-02/diffoscope.svg#right" | relative_url }})](https://diffoscope.org)

[*diffoscope*](https://diffoscope.org) is the Reproducible Build's project in-depth and content-aware diff utility. Not only can it locate and diagnose reproducibility issues, it provides human-readable diffs from many kinds of binary format. This month, [Chris Lamb](https://chris-lamb.co.uk) made a large number of changes (including releasing [version 167](https://diffoscope.org/news/diffoscope-167-released/) and [version 168](https://diffoscope.org/news/diffoscope-168-released/)):

* Bug fixes:

    * Don't call `difflib.Differ.compare` with very large inputs; it is at least [O(n^2)](https://en.wikipedia.org/wiki/Big_O_notation) and makes *diffoscope* (appear to) hang.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/issues/240)]
    * Don't rely on `dumpimage` returning an appropriate exit code; check that the file actually exists.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/a3fbad4)]
    * Don't rely on `magic.Magic` to have an identical API between file's `magic.py` and PyPI's `python-magic` library.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/issues/238)]

* Revamp temporary file handling:

    * Ensure we cleanup our temporary directory by avoiding confusion between the `TemporaryDirectory` instance and the underlying directory.&nbsp;([#981123](https://bugs.debian.org/981123))
    * Try and use a potentially-useful suffix to our temporary directory.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/34477c5)]

* Testsuite improvements:

    * Strip newlines when determining the [Black](https://github.com/psf/black) source code formatter version to avoid `requires black >= 20.8b1 (18.9b0\n detected)` in test output.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/77da137)]
    * Fix [`weakref`](https://docs.python.org/3/library/weakref.html)-related handling in Python 3.7 (i.e. Debian *buster*).&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/issues/239)]
    * If our temporary directory does not exist anymore, recreate it.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/b666c07)]
    * Fix FIT-related tests in Debian *buster*&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/71ce125)] and `fit_expected_diff`&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/e70171a)].
    * [Gnumeric](http://www.gnumeric.org/) is back in testing so re-add to (test) `Build-Depends`.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/7010ead)]
    * Mark `test_apk.py::test_android_manifest` as being allowed to fail for now.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/8fd4b6a)]
    * Add `u-boot-tools` to (test) Build-Depends so [salsa.debian.org](https://salsa.debian.org/) pipelines test the new U-Boot FIT comparator.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/c441700)]
    * Move to `assert_diff` utility in a number of tests.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/c1a43a2)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/4d0c4e4)]

* Codebase improvements:

    * Correct capitalisation of 'jQuery'.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/654bfa8)]
    * Update various copyright years.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/c18be23)]
    * Tidy imports in `diffoscope.comparators.fit`.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/28eec32)]
    * Don't use `Inheriting PATH of X`, use `PATH is X` in logging messages.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/2e6ea68)]
    * Drop unused `Config.acl` and `Config.xattr` attributes&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/f6fc6ce)] and set a default `Config.extended_filesystem_attributes`.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/baddc55)]

Vagrant Cascadian updated *diffoscope* in [GNU Guix](https://guix.gnu.org/) to versions 165&nbsp;[[...](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=b6ad414f4725b96cf799d74bbc11d5dcbb44c75b)], 166,&nbsp;[[...](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=f813750a4aa07797e0120babdd5efbe17f1d3911)] and 167&nbsp;[[...](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=90ccb46a5534a031a6a6d994fd9b7ee2b5ccbf84)].

Mattia Rizzolo updated *diffoscope* in Debian buster-backports to version [166~bpo10+1](https://tracker.debian.org/news/1231967/accepted-diffoscope-166bpo101-source-into-buster-backports/).

### Debian

[![]({{ "/images/reports/2021-02/debian.png#right" | relative_url }})](https://debian.org/)

Roland Clobus created a page on the [Debian Wiki](https://wiki.debian.org/) to detail his progress in [creating reproducible "live" images](https://wiki.debian.org/ReproducibleInstalls/LiveImages) (i.e. bootable USB sticks, etc.). In [Roland's post to our mailing list](https://lists.reproducible-builds.org/pipermail/rb-general/2021-February/002189.html), Roland included a short summary that included:

> The 'standard' image is reproducible, if `fontconfig` and `mdadm` are patched. For `fontconfig` I've created a patch that works for live-build, but not for all other tool that might who need it. For `mdadm` I'm finalizing a patch.

[![]({{ "/images/reports/2021-02/intoto.png#right" | relative_url }})](https://in-toto.io/)

Elsewhere, The [`apt-transport-in-toto`](https://tracker.debian.org/pkg/apt-transport-in-toto) package (an add-on for APT to use [in-toto](https://in-toto.io/) supply-chain verifications), is now available in the *bullseye* distribution for the first time and will, therefore, be included in the next stable release of Debian.

Holger Levsen suggested the creation of a partial mirror of [*snapshot.debian.org*](https://snapshot.debian.org/) (a service needed to rebuild Debian packages) to work around problems with the widespread adoption of the *snapshot.debian.org* site&nbsp;[[...](https://alioth-lists.debian.net/pipermail/reproducible-builds/Week-of-Mon-20210222/012913.html)]. In addition, a new [`metasnap.debian.net`](http://metasnap.debian.net/) service was announced in a [recent edition of Misc Developer News](https://lists.debian.org/debian-devel-announce/2021/02/msg00005.html). This new offering is designed to complement the existing [*snapshot.debian.org*](https://snapshot.debian.org/) service to answer questions such as:

* Given a certain timestamp, which version of a certain package was in a given suite at that&nbsp;time?
* Given a versioned package, in which suite was that package present during which periods of&nbsp;time?
* Given a package and a suite name, which versions where present in that suite during which&nbsp;times?

45 reviews of Debian packages were added, 39 were updated and 28 were removed this month adding to [our knowledge about identified issues](https://tests.reproducible-builds.org/debian/index_issues.html). Two issue types were added by Chris Lamb: [`build_path_in_documentation_generated_by_pdflatex`](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/e85e7d6e) and [`build_path_in_record_file_generated_by_pybuild_flit_plugin`](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/53bbee9b).

### Other distributions

[![]({{ "/images/reports/2021-02/yoctoproject.png#right" | relative_url }})](https://www.yoctoproject.org/)

The [Yocto Project](https://www.yoctoproject.org/) has continued working on improving reproducibility. They now have a [live webpage](https://www.yoctoproject.org/reproducible-build-results/) which shows reproducibility statistics directly from their CI system and have added this to the Reproducible Builds [Continuous tests page](https://reproducible-builds.org/citests/). When the CI system detects differences in the output, it automatically generates [*diffoscope*](https://diffoscope.org) reports and shares these in order to help developers understand the cause of issues and help fix them.

As well as the previously reported `.deb` and `.ipk` output, `.rpm` output is also now being tested in Yocto as well, and for `OpenEmbedded-Core`, 34,335 out of 34,392 packages are now reproducible. The differences are limited to code using the [Go](https://golang.org/) programming language (which isn't reproducible at present), `perf` and three other packages which are exhibiting minor issues.

[![]({{ "/images/reports/2021-02/opensuse.png#right" | relative_url }})](https://www.opensuse.org/)

Bernhard M. Wiedemann posted his [monthly reproducible builds status report](https://lists.opensuse.org/archives/list/factory@lists.opensuse.org/thread/3A6DKFPDDRLPZBEUSHD234RHHQ77AZCH/) for the [openSUSE](https://www.opensuse.org/) distribution which had a number of followups on the topic of unique identifiers in PDF files and [`SOURCE_DATE_EPOCH`]({{ "/docs/source-date-epoch/" | relative_url }}). Bernhard also packaged [dettrace](https://github.com/dettrace/dettrace) (covered in [a previous month's report]({{ "/reports/2020-02/" }})) for openSUSE too&nbsp;[[...](https://build.opensuse.org/request/show/869868)].

[![]({{ "/images/reports/2021-02/qubes.png#right" | relative_url }})](https://www.qubes-os.org/)

Marek Marczykowski-Górecki wrote a lengthy blog post about the development process of [Qubes-OS](https://www.qubes-os.org/) titled "[*Improvements in testing and building: GitLab CI and reproducible builds*](https://www.qubes-os.org/news/2021/02/28/improvements-in-testing-and-building/)". Marek describes the problem solved by reproducible builds as follows:

> [Imagine] that an attacker wishes to feed unsuspecting users a compromised package. The attacker knows that the source code is public, so any malicious code he inserts into it would be highly exposed and at risk of detection. On the other hand, he reasons, compromising the build infrastructure would allow him to surreptitiously insert malicious changes that would make it into the resultant package. Since the source code remains untouched, his malicious changes are less likely to be detected. This is where the value of reproducible builds comes in. If the build process is reproducible, then we will immediately notice that building a package from the untouched source code results in a package that is different from the compromised one. This would be a major red flag that would prompt an immediate security investigation.&nbsp;[[...](https://www.qubes-os.org/news/2021/02/28/improvements-in-testing-and-building/)]

In [Fedora](https://fedoraproject.org/), Frédéric Pierret [restarted a discussion regarding `.buildinfo` files for RPM]( https://lists.fedoraproject.org/archives/list/devel@lists.fedoraproject.org/thread/OLCD2L6I5KUKWR6WLIUXFUYZ7KAQ66E3/), and made [*disorderfs*](https://salsa.debian.org/reproducible-builds/disorderfs) and [*reprotest*](https://salsa.debian.org/reproducible-builds/reprotest) available in the official Fedora repos.

[![]({{ "/images/reports/2021-02/nixos.png#right" | relative_url }})](https://nixos.org/)

In [NixOS](https://nixos.org), Tom Berek made the [date in the asciidoc manpages](https://github.com/NixOS/nixpkgs/pull/111572/files) deterministic and Arnout Engelen made sure that [squashfs images are reproducible](https://github.com/NixOS/nixpkgs/pull/114454), regardless of the presence of hard links. For the milestone of a fully-reproducible minimal installation ISO include open PRs for [`gcc`](https://github.com/NixOS/nixpkgs/pull/112928) and [`python`](https://github.com/NixOS/nixpkgs/pull/107965).

### Upstream patches

The Reproducible Builds project detects, dissects and attempts to fix as many currently-unreproducible packages as possible. We endeavour to send all of our patches upstream where appropriate. This month, we wrote a large number of such patches, including:

* Bernhard M. Wiedemann:

    * [`automake/pcre`](https://bugzilla.opensuse.org/show_bug.cgi?id=1182604) (filesystem and [ASLR](https://en.wikipedia.org/wiki/Address_space_layout_randomization)-related issue))
    * [`calc`](https://build.opensuse.org/request/show/872190) (required fixes for non-Intel CPUs)
    * [`cpan`](https://github.com/aferreira/cpan-Term-Size-Perl/pull/1) (date-related issue)
    * [`gmic`](https://github.com/dtschump/gmic/pull/288) (address a copyright year)
    * [`gocr`](https://build.opensuse.org/request/show/875812) (randomisation issue)
    * [`HTTP`](https://github.com/dagolden/HTTP-CookieJar/pull/10) (fix build failures after June 2021)
    * [`ibus`](https://build.opensuse.org/request/show/868399) (parallelism issue)
    * [`ipxe`](https://github.com/ipxe/ipxe/pull/234) (date issue, random issue)
    * [`ipxe`](https://github.com/ipxe/ipxe/pull/252) (modification time issue)
    * [`jpype`](https://github.com/jpype-project/jpype/pull/931) (sort a Python-based filesystem ordering)
    * [`lagrange`](https://build.opensuse.org/request/show/873004) (CPU-related issue)
    * [`libsoup`](https://gitlab.gnome.org/GNOME/libsoup/-/merge_requests/177) (fix build failing in 2027)
    * [`openscap`](https://github.com/OpenSCAP/openscap/pull/1699) (modification time issue)
    * `scap-security-guide`:
        * Sort a Python-based filesystem ordering.&nbsp;[[...](https://github.com/ComplianceAsCode/content/pull/6647)]
        * Date-related issue.&nbsp;[[...](https://github.com/ComplianceAsCode/content/pull/6642)]
    * [`syslinux/isohybrid`](https://build.opensuse.org/request/show/868912) (fix a nondeterministic [MBR](https://en.wikipedia.org/wiki/Master_boot_record) ID for `ipxe.iso`)

* Chris Lamb:

    * [#981570](https://bugs.debian.org/981570) filed against [`crossfire`](https://tracker.debian.org/pkg/crossfire).
    * [#981571](https://bugs.debian.org/981571) filed against [`zmk`](https://tracker.debian.org/pkg/zmk).
    * [#982529](https://bugs.debian.org/982529) filed against [`python-aiosqlite`](https://tracker.debian.org/pkg/python-aiosqlite).
    * [#982851](https://bugs.debian.org/982851) filed against [`mocassin`](https://tracker.debian.org/pkg/mocassin) ([forwarded upstream](https://github.com/rwesson/mocassin/pull/13)).
    * [#983033](https://bugs.debian.org/983033) filed against [`golang-github-revel-revel`](https://tracker.debian.org/pkg/golang-github-revel-revel).
    * [#983046](https://bugs.debian.org/983046) filed against [`kjs`](https://tracker.debian.org/pkg/kjs).
    * [#983163](https://bugs.debian.org/983163) filed against [`golang-github-viant-toolbox`](https://tracker.debian.org/pkg/golang-github-viant-toolbox).

* Vagrant Cascadian:

    * [#983126](https://bugs.debian.org/983126) filed against [`iptotal`](https://tracker.debian.org/pkg/iptotal).
    * [#983138](https://bugs.debian.org/983138) filed against [`ypserv`](https://tracker.debian.org/pkg/ypserv).
    * [#983142](https://bugs.debian.org/983142) filed against [`circlator`](https://tracker.debian.org/pkg/circlator).
    * [#983147](https://bugs.debian.org/983147) filed against [`armagetronad`](https://tracker.debian.org/pkg/armagetronad).
    * [#983148](https://bugs.debian.org/983148) filed against [`wxmaxima`](https://tracker.debian.org/pkg/wxmaxima).
    * [#983202](https://bugs.debian.org/983202) filed against [`time`](https://tracker.debian.org/pkg/time).
    * [#983208](https://bugs.debian.org/983208) & [#983209](https://bugs.debian.org/983209) filed against [`lynx`](https://tracker.debian.org/pkg/lynx).
    * [#983302](https://bugs.debian.org/983302) & [#983303](https://bugs.debian.org/983303) filed against [`imagemagick`](https://tracker.debian.org/pkg/imagemagick).
    * [#983584](https://bugs.debian.org/983584) filed against [`paraview`](https://tracker.debian.org/pkg/paraview).
    * [#983588](https://bugs.debian.org/983588) filed against [`xmlgraphics-commons`](https://tracker.debian.org/pkg/xmlgraphics-commons).

### Testing framework

[![]({{ "/images/reports/2021-02/testframework.png#right" | relative_url }})](https://tests.reproducible-builds.org/)

The Reproducible Builds project operates a [Jenkins](https://jenkins.io/)-based testing framework that powers [`tests.reproducible-builds.org`](https://tests.reproducible-builds.org). This month, the following changes were made:

* Frédéric Pierret ([Qubes-OS](https://www.qubes-os.org/)):

    * Add a new `buildinfos_suites` job.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/4750f260)]
    * Adjust the `ARCHES` and per-suite list for our new job.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/82e34937)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/7020347f)]

* Holger Levsen:

    * Switch the `ionos7` host to Debian *bullseye*&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/9ca30125)] and update the [PostgreSQL](https://www.postgresql.org/)-related packages for a `.buildinfo`-related service hosted on Debian *bullseye* too&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/8fdfbdf3)].
    * Improve the `deploy_jdn` script, adding support for short options&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/01a5042d)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/27374dcc)], conditional deployment&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/c26ea9f7)] and some general code improvements&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/23a9bcd2)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/b108293d)].
    * Fix failed networking and "`pbuilder_create` scope" issues in the node health check system.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/ac45dd4c)]
    * Move more IRC notifications to the `#reproducible-changes` channel&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/ce6caae6)] and be verbose about sleeping time.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/d8c3c736)]

    * Package rebuilder prototype:

        * Drop a reference and workaround to Debian bug related to signed `.buildinfo` files ([#955050](https://bugs.debian.org/955050)) as it has been fixed upstream.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/2202e4ab)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/d452797d)]
        * Remove a workaround that was previously needed for the version of [`sbuild`](https://tracker.debian.org/pkg/sbuild) in Debian *buster*.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/190416c6)]
        * Use `debrebuild --builder=sbuild` to better mimic the behaviour of the [official Debian build servers](https://buildd.debian.org/).&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/62524892)]
        * Make some miscellaneous code improvements.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/24bb6a1a)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/9e4ebffc)]

Lastly, build node maintenance was performed by Holger Levsen&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/beecf594)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/d3ef2405)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/87f84a05)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/7d410b25)], Mattia Rizzolo&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/f24efd32)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/6e0a1fc6)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/342d19d7)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/6c4e167f)] and Vagrant Cascadian&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/34886eb4)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/90940d17)].

### Other development news

[On our website this month]({{ "/" | relative_url }}), Holger Levsen added a public [`reproducible-builds-developers-keys.asc`](https://reproducible-builds.org/reproducible-builds-developers-keys.asc) file which contains the GPG keys used by some Reproducible Builds developers&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/140de8d)] and Joshua Watt added a link to [Yocto Project](https://www.yoctoproject.org/)'s reproducible builds summary.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/3e5c7b0)]

[*strip-nondeterminism*](https://tracker.debian.org/pkg/strip-nondeterminism) is our tool to remove specific non-deterministic results from a completed build. This month, Chris Lamb [uploaded version `1.11.0-1` to Debian unstable](https://tracker.debian.org/news/1228381/accepted-strip-nondeterminism-1110-1-source-into-unstable/), notably to include a contribution from Helmut Grohne in order to normalise `PO-Revision-Date` fields (in addition to `POT-Creation-Date`) in [GNU *gettext*](https://www.gnu.org/software/gettext/) translation ifiles ([#981895](https://bugs.debian.org/981895)).

In a thread on [our mailing list](https://lists.reproducible-builds.org/listinfo/rb-general) which was started to discuss potential ideas for [Outreachy](https://www.outreachy.org/), Chris Lamb mentioned that he had been working on a proof-of-concept for a tool to automatically classify issues from the output of [diffoscope](https://diffoscope.org) and has [added it to the `reproducible-notes.git` repository](https://salsa.debian.org/reproducible-builds/reproducible-notes/-/blob/master/bin/auto-classify).&nbsp;[[...](https://lists.reproducible-builds.org/pipermail/rb-general/2021-February/thread.html#2193)]

[*reprotest*](https://tracker.debian.org/pkg/reprotest) is the Reproducible Build's project end-user tool to build same source code twice in widely differing environments, checking the binaries produced by the builds for any differences. This month, Frédéric Pierret made a number of changes to its RPM spec file [[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/fca02bb)][[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/4ac84ed)] and improved the testsuite in a handful of ways&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/6a51832)][[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/0c17de7)]. Vagrant Cascadian then updated the version in [GNU Guix](https://guix.gnu.org/).&nbsp;[[...](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=8692f0e73b200638fd8bd01fecad068903cfa77a)]

<br>

[![]({{ "/images/reports/2021-02/reproducible-builds.png#right" | relative_url }})](https://reproducible-builds.org/)

If you are interested in contributing to the Reproducible Builds project, please visit our [*Contribute*](https://reproducible-builds.org/contribute/) page on our website. However, you can get in touch with us via:

 * IRC: `#reproducible-builds` on `irc.oftc.net`.

 * Twitter ([@ReproBuilds](https://twitter.com/ReproBuilds)) & Mastodon ([@reproducible_builds@fosstodon.org](https://fosstodon.org/@reproducible_builds))

 * Reddit: [/r/ReproducibleBuilds](https://reddit.com/r/reproduciblebuilds)

 * Mailing list: [`rb-general@lists.reproducible-builds.org`](https://lists.reproducible-builds.org/listinfo/rb-general)
